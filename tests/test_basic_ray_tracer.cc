#include "ray_tracing_sandbox/render_engine.h"


int main(int argc, char* argv[]) {
  std::string output_folder = "C:/These_CAOR/projects/ray_tracing_sandbox/log";
  std::string output_name = "test_bin";
  int width = 1280;
  int height = 720;

  ray_tracing::RenderEngine renderer(width, height);
  renderer.Render(output_folder + "/" + output_name);

  return 0;
}