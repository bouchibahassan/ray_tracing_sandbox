#include <fstream>
#include <iostream>
#include <string>


int main(int argc, char* argv[]) {
  std::cout << "Testing PPM image format..." << std::endl;

  std::string folder = "C:/These_CAOR/projects/ray_tracing_sandbox/log";
  std::ofstream file_out(folder + "/test.ppm");

  if (!file_out.good()) {
    std::cout << "can't open file" << std::endl;
    return 1;
  }

  int width = 1080;
  int height = 720;

  // Writing header
  file_out << "P3" << std::endl;
  file_out << width << " " << height << std::endl;
  file_out << 255 << std::endl;

  // Writing per pixel information
  for (int j(0); j<height; ++j) {
    for (int i(0); i<width; ++i) {
      file_out << static_cast<int>( 255.0f *
                  static_cast<float>(i) / 
                  static_cast<float>(width) );
      file_out << " ";
      file_out << static_cast<int>( 255.0f *
                  static_cast<float>(j) / 
                  static_cast<float>(height) );
      file_out << " ";
      file_out << "0 ";
    }
    file_out << std::endl;
  }

  return 0;
}