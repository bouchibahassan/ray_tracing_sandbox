#include <fstream>
#include <iostream>
#include <string>
#include <ray_tracing_sandbox/base_types/ray.h>
#include <ray_tracing_sandbox/rt_objects/rt_object.h>
#include <ray_tracing_sandbox/rt_objects/rt_sphere.h>


int main(int argc, char* argv[]) {
  std::cout << "Testing intersection..." << std::endl;

  ray_tracing::RTObject* object;
  object = &ray_tracing::RTSphere(Eigen::Vector3d(1.0, 0.0 ,0.0), 1.0);
  
  auto ray_origin = Eigen::Vector3d(1.0, 0.8 ,1.0);
  auto ray_direction = Eigen::Vector3d(-1.0, -1.0 ,-1.0);
  ray_direction.normalize();
  ray_tracing::Ray test_ray{ray_origin,
                            ray_direction};
  std::cout << "ray origin: " << std::endl 
            << ray_origin << std::endl;
  std::cout << "ray direction: " << std::endl
            << ray_direction << std::endl;

  Eigen::Vector3d inter_point(0.0, 0.0, 0.0); 
  Eigen::Vector3d inter_normal(0.0, 0.0, 0.0);
  bool is_intersecting = object->Intersect(test_ray, 
                                           inter_point, 
                                           inter_normal);

  std::cout << "intersect: " << is_intersecting << std::endl;
  std::cout << "intersect point: " << std::endl 
            << inter_point << std::endl;
  std::cout << "intersect normal: " << std::endl
            << inter_normal << std::endl;
  return 0;
}