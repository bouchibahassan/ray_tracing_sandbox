#include <fstream>
#include <iostream>
#include <string>
#include <ray_tracing_sandbox/base_types/ray.h>
#include <ray_tracing_sandbox/rt_objects/rt_object.h>
#include <ray_tracing_sandbox/rt_objects/rt_sphere.h>
#include <ray_tracing_sandbox/camera/camera.h>


int main(int argc, char* argv[]) {
  std::cout << "Testing camera..." << std::endl;

  ray_tracing::RTObject* object;
  object = &ray_tracing::RTSphere(Eigen::Vector3d(10.0, 0.0 ,0.0), 1.0);

  Eigen::Vector3d inter_point(0.0, 0.0, 0.0); 
  Eigen::Vector3d inter_normal(0.0, 0.0, 0.0);

  ray_tracing::Camera camera(4, 2, 60.0 * M_PI/180.0);
  camera.LookAt(Eigen::Vector3d(0.0, 0.0, 0.0),
                Eigen::Vector3d(1.0, 0.0, 0.0),
                Eigen::Vector3d(0.0, 0.0, 1.0));

  return 0;
}