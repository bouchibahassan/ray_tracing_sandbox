set BUILD_DIR=_buildVS2013x64

if exist %BUILD_DIR% (rd /S /Q %BUILD_DIR%)
mkdir %BUILD_DIR%
cd %BUILD_DIR%

cmake .. -G "Visual Studio 12 2013 Win64"

pause