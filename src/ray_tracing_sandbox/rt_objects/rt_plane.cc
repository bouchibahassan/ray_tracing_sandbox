#include <iostream>
#include "ray_tracing_sandbox/rt_objects/rt_plane.h"


bool ray_tracing::RTPlane::Intersect(const Ray& incoming_ray, 
    Eigen::Vector3d& inter_point, 
    Eigen::Vector3d& inter_normal,
    double& dist_to_center) const {
  // Vector PO (from point_ to the origin of the ray to)
  Eigen::Vector3d point_origin = incoming_ray.origin - point_;
  // PO dot plane normal
  double point_origin_dot_normal = point_origin.dot(normal_);
  // Ray direction dot plane normal
  double direction_dot_normal = incoming_ray.direction.dot(normal_);

  if (direction_dot_normal == 0) {  // ray is parallel to the plane
    return false;
  } else {
    double x = -point_origin_dot_normal / direction_dot_normal;
    if (x > 0.0) {
      inter_point = incoming_ray.origin
                    + x * incoming_ray.direction;
      // needed if the ray hits the plane from behind
      if (direction_dot_normal<0) {
        inter_normal = normal_;
      } else {
        inter_normal = -normal_;
      }
      dist_to_center = x;
      return true;        
    } else {
      return false;
    }
  }
}