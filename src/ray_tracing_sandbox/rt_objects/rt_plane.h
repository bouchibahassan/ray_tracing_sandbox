#ifndef RAY_TRACING_SANDBOX_RT_OBJECTS_PLANE_H_
#define RAY_TRACING_SANDBOX_RT_OBJECTS_PLANE_H_

#include <Eigen/Dense>
#include "ray_tracing_sandbox/base_types/ray.h"
#include "ray_tracing_sandbox/rt_objects/rt_object.h"


namespace ray_tracing {

// Analytic model of an infinite plane
class RTPlane : public RTObject {
 public:
  // Constructor
  RTPlane(const Eigen::Vector3d& point, const Eigen::Vector3d& normal) 
      : point_(point),
        normal_(normal) {
    normal_.normalize();
  }

  // Returns the closest intersection point and normal to the ray with 
  // the plane.
  bool Intersect(const Ray& incoming_ray, 
                 Eigen::Vector3d& inter_point, 
                 Eigen::Vector3d& inter_normal,
                 double& dist_to_center) const;
  
 private:
  Eigen::Vector3d point_;
  Eigen::Vector3d normal_;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_RT_OBJECTS_PLANE_H_