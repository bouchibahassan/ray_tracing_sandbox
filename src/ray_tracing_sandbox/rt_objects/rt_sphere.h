#ifndef RAY_TRACING_SANDBOX_RT_OBJECTS_SPHERE_H_
#define RAY_TRACING_SANDBOX_RT_OBJECTS_SPHERE_H_

#include <Eigen/Dense>
#include "ray_tracing_sandbox/base_types/ray.h"
#include "ray_tracing_sandbox/rt_objects/rt_object.h"


namespace ray_tracing {

// Analytic model of a shpere
class RTSphere : public RTObject {
 public:
  // Constructor
  RTSphere(const Eigen::Vector3d& center, double radius) 
      : center_(center),
        radius_(radius) {}

  // Returns the closest intersection point and normal to the ray with 
  // the sphere.
  bool Intersect(const Ray& incoming_ray, 
                 Eigen::Vector3d& inter_point, 
                 Eigen::Vector3d& inter_normal,
                 double& dist_to_center) const;
  
 private:
  Eigen::Vector3d center_;
  double radius_;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_RT_OBJECTS_SPHERE_H_