#include <iostream>
#include "ray_tracing_sandbox/rt_objects/rt_sphere.h"


bool ray_tracing::RTSphere::Intersect(const Ray& incoming_ray, 
    Eigen::Vector3d& inter_point, 
    Eigen::Vector3d& inter_normal,
    double& dist_to_center) const {
  // Vector from the center of the sphere to the origin of the ray
  Eigen::Vector3d center_origin = incoming_ray.origin - center_;

  double dot_prod = center_origin.dot(incoming_ray.direction);
  double delta =   pow(radius_,2)
                 + pow(dot_prod,2) 
                 - center_origin.squaredNorm();

  if (delta < 0) {
    return false;
  } else {
    double x_1 = -dot_prod-sqrt(delta);
    double x_2 = -dot_prod+sqrt(delta);
    if (x_2 < 0) {
      return false;
    } else if (x_1 < 0) {
      inter_point =   incoming_ray.origin
                    + x_2 * incoming_ray.direction;
      inter_normal = inter_point - center_;
      inter_normal.normalize();
      dist_to_center = x_2;
      return true;
    } else {
      inter_point =   incoming_ray.origin
                    + x_1 * incoming_ray.direction;
      inter_normal = inter_point - center_;
      inter_normal.normalize();
      dist_to_center = x_1;
      return true;
    }
  }
}