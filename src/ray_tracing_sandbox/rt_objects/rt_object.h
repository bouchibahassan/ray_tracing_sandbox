#ifndef RAY_TRACING_SANDBOX_3D_OBJECT_H_
#define RAY_TRACING_SANDBOX_3D_OBJECT_H_

#include <Eigen/Dense>
#include "ray_tracing_sandbox/base_types/ray.h"


namespace ray_tracing {

// Virtual class to handle generic "Ray Tracable" 3D object
class RTObject {
 public:
  // Given an incoming ray, this class returns the intersection point 
  // and the normal to the surface.
  // The intersection point returned should be the closest to the ray 
  // origin.
  virtual bool Intersect(const Ray& incoming_ray, 
                         Eigen::Vector3d& inter_point, 
                         Eigen::Vector3d& inter_normal,
                         double& dist_to_center) const = 0;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_3D_OBJECT_H_