#include <vector>
#include <Eigen/Dense>
#include "ray_tracing_sandbox/scene.h"
#include "ray_tracing_sandbox/rt_lights/rt_light.h"
#include "ray_tracing_sandbox/rt_objects/rt_object.h"


double ray_tracing::Scene::kIntersectionEpsilon = 0.0001;

bool ray_tracing::Scene::Intersect(const Ray& incoming_ray, 
    Eigen::Vector3d& inter_point, 
    Eigen::Vector3d& inter_normal,
    double& dist_to_center) {
  Eigen::Vector3d tmp_inter_point(0.0, 0.0, 0.0); 
  Eigen::Vector3d tmp_inter_normal(0.0, 0.0, 0.0); 
  
  dist_to_center = std::numeric_limits<double>::max();
  double tmp_dist_to_center(0.0);
  bool intersect(false);

  for (auto& obj: objects_) {
    if (obj->Intersect(incoming_ray, tmp_inter_point, 
                       tmp_inter_normal, tmp_dist_to_center)) {
      intersect = true;
      if ( (tmp_dist_to_center > kIntersectionEpsilon) &&
           (tmp_dist_to_center < dist_to_center) ) {
        inter_point = tmp_inter_point;
        inter_normal = tmp_inter_normal;
        dist_to_center = tmp_dist_to_center;
      }
    };
  } 
  
  return intersect;            
}

  void ray_tracing::Scene::Lighting(Eigen::Vector3d& point,
    std::vector<Eigen::Vector3d>& light_vectors,
    std::vector<Eigen::Vector3d>& light_intensities,
    std::vector<double>& light_dists) {
   light_vectors.clear();
   light_intensities.clear();
   light_dists.clear();
   
   Eigen::Vector3d tmp_ligth_vector;
   Eigen::Vector3d tmp_light_intensity;
   double tmp_light_dist;

   for (auto& light: lights_) {
     light->Lighting(point, tmp_ligth_vector, 
                     tmp_light_intensity, tmp_light_dist);
     light_vectors.push_back(tmp_ligth_vector);
     light_intensities.push_back(tmp_light_intensity);
     light_dists.push_back(tmp_light_dist);
   }               
 }