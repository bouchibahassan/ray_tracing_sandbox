#ifndef RAY_TRACING_SANDBOX_RENDER_ENGINE_H_
#define RAY_TRACING_SANDBOX_RENDER_ENGINE_H_

#include <string>
#include "ray_tracing_sandbox/scene.h"
#include "ray_tracing_sandbox/base_types/image.h"
#include "ray_tracing_sandbox/camera/camera.h"
#include "ray_tracing_sandbox/utils/timer.h"


namespace ray_tracing {

class RenderEngine {
 public:
  RenderEngine(int width, int height) 
      : width_(width),
        height_(height),
        camera_(width, height) {}
  // Render the scene to the specified file
  void Render(std::string path_to_output_image);

 private:
  int width_;
  int height_;
  Camera camera_;
  Scene scene_;
};

}

#endif  // RAY_TRACING_SANDBOX_RENDER_ENGINE_H_