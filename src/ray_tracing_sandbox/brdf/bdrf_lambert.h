#ifndef RAY_TRACING_SANDBOX_BRDF_BRDF_LAMBERT_H_
#define RAY_TRACING_SANDBOX_BRDF_BRDF_LAMBERT_H_

#include <Eigen/Dense>


namespace ray_tracing {

class BRDFLambert {
 public:
  Eigen::Vector3d Color();
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_BRDF_BRDF_LAMBERT_H_