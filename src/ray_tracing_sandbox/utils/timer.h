#ifndef RAY_TRACING_SANDBOX_UTILS_TIMER_H
#define RAY_TRACING_SANDBOX_UTILS_TIMER_H

#include <ctime>
#include <iostream>
#include <string>


namespace ray_tracing {

// Simple timer class
// usage :
//   Timer timer();
//   timer.Tic();
//   // some functions here
//   std::cout << timer.Toc() << std::endl;
class Timer {
 public:
  // Consturctor
  Timer() : time_(0.0) {}
  
  // Begin the timing
  void Tic() {
    time_ = static_cast<double>(clock());
  }

  // Ends the timing and returns the elapsed time in seconds
  double Toc() {
    return static_cast<double>(clock() - time_) /
           static_cast<double>(CLOCKS_PER_SEC);
  }

  // Ends the timing, returns and prints the elapsed time in seconds.
  // With the label value, one can print a specific message
  double Toc(std::string label) {
    double delta_time = Toc();
    std::cout << "Elapsed time (" << label << ") : " 
              << delta_time << std::endl;

    return delta_time;
  }

 private:
  double time_;
};

}  // namespace ray_tracing

#endif  // RAY_TRACING_SANDBOX_UTILS_TIMER_H