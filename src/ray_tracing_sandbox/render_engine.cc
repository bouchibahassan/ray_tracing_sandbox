#include "ray_tracing_sandbox/render_engine.h"
#include "ray_tracing_sandbox/rt_lights/rt_point_light.h"
#include "ray_tracing_sandbox/rt_lights/rt_directional_light.h"
#include "ray_tracing_sandbox/rt_lights/rt_spot_light.h"
#include "ray_tracing_sandbox/rt_objects/rt_plane.h"
#include "ray_tracing_sandbox/rt_objects/rt_sphere.h"
#include "ray_tracing_sandbox/utils/timer.h"


void ray_tracing::RenderEngine::Render(std::string path_to_output_image) {
  ray_tracing::Timer timer;

  // ----- Create scene --------------------------------------------------- //
  timer.Tic();
  // 3D objects
  RTPlane plane_0(Eigen::Vector3d(0.0, 0.0, 0.0), 
                  Eigen::Vector3d(0.0, 0.0, 1.0));
  RTPlane plane_1(Eigen::Vector3d(15.0, 0.0, 0.0), 
                  Eigen::Vector3d(1.0, 0.0, 0.0));
  RTPlane plane_2(Eigen::Vector3d(0.0, 7.5, 0.0), 
                  Eigen::Vector3d(0.0, 1.0, 0.0));
  RTPlane plane_3(Eigen::Vector3d(0.0, -7.5, 0.0), 
                  Eigen::Vector3d(0.0, 1.0, 0.0));
  RTSphere shpere_0(Eigen::Vector3d(10.0, 0.0 ,1.3), 1.3);
  RTSphere shpere_1(Eigen::Vector3d(8.0, 4.0 ,1.0), 1.0);
  RTSphere shpere_2(Eigen::Vector3d(7.0, -3.0 ,0.5), 0.5);
  scene_.AddObject(plane_0);
  //scene_.AddObject(plane_1);
  //scene_.AddObject(plane_2);
  //scene_.AddObject(plane_3);
  scene_.AddObject(shpere_0);
  scene_.AddObject(shpere_1);
  scene_.AddObject(shpere_2);

  // lights
  RTPointLight point_light_0(Eigen::Vector3d(0.0, -3.0, 10.0), 
                             Eigen::Vector3d(1.0, 1.0, 1.0));
  RTDirectionalLight directional_light_0(Eigen::Vector3d(0.0, 3.0, -10.0), 
                                         Eigen::Vector3d(1.0, 1.0, 1.0));
  RTSpotLight spot_light_0(Eigen::Vector3d(0.0, -3.0, 10.0), 
                           Eigen::Vector3d(1.0, 1.0, 1.0),
                           Eigen::Vector3d(8.0, 0.0, 0.0),
                           20.0*M_PI/180.0,
                           5.0*M_PI/180.0);
  scene_.AddLight(point_light_0);
  //scene_.AddLight(directional_light_0);
  //scene_.AddLight(spot_light_0);

  // camera
  camera_.LookAt(Eigen::Vector3d(0.0, 0.0, 3.0),
                 Eigen::Vector3d(10.0, 0.0, 1.0),
                 Eigen::Vector3d(0.0, 0.0, 1.0));
  

  // ----- Render scene --------------------------------------------------- //
  ray_tracing::Image image(width_, height_);
  ray_tracing::Image depth_map(width_, height_);
  ray_tracing::Image normal_map(width_, height_);

  Eigen::Vector3d cam_pos = camera_.position();
  Eigen::Vector3d inter_point(0.0, 0.0, 0.0); 
  Eigen::Vector3d inter_normal(0.0, 0.0, 0.0);
  Eigen::Vector3d tmp_point(0.0, 0.0, 0.0); 
  Eigen::Vector3d tmp_normal(0.0, 0.0, 0.0);
  double ray_dist(0.0);
  double tmp_dist(0.0);
  std::vector<Eigen::Vector3d> light_vectors;
  std::vector<Eigen::Vector3d> light_intensities;
  std::vector<double> light_dists;
  
  for (int j(0); j<height_; ++j) {
    for (int i(0); i<width_; ++i) {
      Pixel cam_pixel = camera_.pixel(i,j); 
      Eigen::Vector3d ray_direction = cam_pixel.center - cam_pos;
      ray_direction.normalize();
      Ray ray{cam_pos, ray_direction};
      
      if (scene_.Intersect(ray, inter_point, inter_normal, ray_dist)) {
        normal_map.pixel(i,j) = 0.5 * (inter_normal + Eigen::Vector3d(1.0, 1.0, 1.0));
        depth_map.pixel(i,j) = Eigen::Vector3d(ray_dist, ray_dist, ray_dist);
        
        scene_.Lighting(inter_point, light_vectors, 
                        light_intensities, light_dists);
        
        // compute shadowing
        scene_.Intersect(Ray{inter_point,light_vectors[0]}, 
                         tmp_point, tmp_normal, tmp_dist);
        if (tmp_dist<light_dists[0]) {
          light_intensities[0]=0.0*light_intensities[0];
        }

        image.pixel(i,j) = std::max(light_vectors[0].dot(inter_normal),0.0)
                           * light_intensities[0];
      }
    }
  } 
  timer.Toc("ray tracing");

  timer.Tic();
  image.ExportPPM(path_to_output_image + ".ppm", 
                  PPMFormat::kBinary);
  normal_map.ExportPPM(path_to_output_image + "_normal_map.ppm", 
                       PPMFormat::kBinary);
  depth_map.ExportPPM(path_to_output_image + "_depth_map.ppm", 
                      PPMFormat::kBinary);

  timer.Toc("image export");
}
