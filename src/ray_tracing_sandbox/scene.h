#ifndef RAY_TRACING_SANDBOX_SCENE_H_
#define RAY_TRACING_SANDBOX_SCENE_H_

#include <vector>
#include <Eigen/Dense>
#include "ray_tracing_sandbox/rt_lights/rt_light.h"
#include "ray_tracing_sandbox/rt_objects/rt_object.h"


namespace ray_tracing {

class Scene {
 public:
  // Epsilon value for ignoring an intersection value
  static double kIntersectionEpsilon;

  // Compute the intersection of a ray with all elements of the scene and
  // returns the closest intersection point (and its normal) to the ray 
  // origin
  bool Intersect(const Ray& incoming_ray, 
                 Eigen::Vector3d& inter_point, 
                 Eigen::Vector3d& inter_normal,
                 double& dist_to_center);
  // Compute the light vectors, light intensities and light distances for all 
  // the light sources of the scene
  void Lighting(Eigen::Vector3d& point,
                std::vector<Eigen::Vector3d>& light_vectors,
                std::vector<Eigen::Vector3d>& light_intensities,
                std::vector<double>& light_dists);

  // Adds an object to the scene
  void AddObject(RTObject& object) {objects_.push_back(&object);}

  // Adds a light to the scene
  void AddLight(RTLight& light) {lights_.push_back(&light);};

 private:
  std::vector<RTObject*> objects_;
  std::vector<RTLight*> lights_;
};

}

#endif  // RAY_TRACING_SANDBOX_SCENE_H_