#ifndef RAY_TRACING_SANDBOX_RT_LIGHTS_RT_LIGHT_H_
#define RAY_TRACING_SANDBOX_RT_LIGHTS_RT_LIGHT_H_

#include <Eigen/Dense>


namespace ray_tracing {

class RTLight {
 public:
  // Virtual function.
  // Given a point in 3D space (for example the result of the intersection 
  // with a Ray) this function returns the vector from the light to this point
  // (normalized), the light intensity and the light distance.
  virtual void Lighting(Eigen::Vector3d& point,
                        Eigen::Vector3d& light_vector,
                        Eigen::Vector3d& light_intensity,
                        double& light_dist) = 0;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_RT_LIGHTS_RT_LIGHT_H_