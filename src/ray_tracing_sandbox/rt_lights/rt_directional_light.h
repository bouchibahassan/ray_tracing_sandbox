#ifndef RAY_TRACING_SANDBOX_RT_LIGHTS_RT_DIRECTIONAL_LIGHT_H_
#define RAY_TRACING_SANDBOX_RT_LIGHTS_RT_DIRECTIONAL_LIGHT_H_

#include <Eigen/Dense>
#include "ray_tracing_sandbox/rt_lights/rt_light.h"


namespace ray_tracing {

// A directional light is equivalent to a point light very far from the scene 
// (e.g : the sun)
class RTDirectionalLight : public RTLight {
 public:
  RTDirectionalLight(Eigen::Vector3d direction, Eigen::Vector3d col)
      : direction_(direction),
        color_(col) {
    direction_.normalize();
  }

  // Given a point in 3D space (for example the result of the intersection 
  // with a Ray) this function returns the vector from the light to this point
  // (normalized), the light intensity and the light distance.
  void Lighting(Eigen::Vector3d& point,
                Eigen::Vector3d& light_vector,
                Eigen::Vector3d& light_intensity,
                double& light_dist)  {
    light_vector = -direction_;
    light_dist = std::numeric_limits<double>::max();
    light_intensity = color_;
  }
   
 private:
  Eigen::Vector3d direction_;
  Eigen::Vector3d color_;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_RT_LIGHTS_RT_DIRECTIONAL_LIGHT_H_