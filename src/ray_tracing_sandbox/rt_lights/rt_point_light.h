#ifndef RAY_TRACING_SANDBOX_LIGHT_POINT_LIGHT_H_
#define RAY_TRACING_SANDBOX_LIGHT_POINT_LIGHT_H_

#include <Eigen/Dense>
#include "ray_tracing_sandbox/rt_lights/rt_light.h"


namespace ray_tracing {

class RTPointLight : public RTLight {
 public:
  RTPointLight(Eigen::Vector3d pos, Eigen::Vector3d col)
      : position_(pos),
        color_(col) {}

  // Given a point in 3D space (for example the result of the intersection 
  // with a Ray) this function returns the vector from the light to this point
  // (normalized), the light intensity and the light distance.
  void Lighting(Eigen::Vector3d& point,
                Eigen::Vector3d& light_vector,
                Eigen::Vector3d& light_intensity,
                double& light_dist)  {
    light_vector = position_ - point;
    light_dist = light_vector.norm();
    light_vector.normalize();
    light_intensity = color_;
  }
   
 private:
  Eigen::Vector3d position_;
  Eigen::Vector3d color_;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_LIGHT_POINT_LIGHT_H_