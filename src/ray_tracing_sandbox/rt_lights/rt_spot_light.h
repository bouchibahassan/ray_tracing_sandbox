#ifndef RAY_TRACING_SANDBOX_RT_LIGHTS_RT_SPOT_LIGHT_H_
#define RAY_TRACING_SANDBOX_RT_LIGHTS_RT_SPOT_LIGHT_H_

#include <Eigen/Dense>
#include "ray_tracing_sandbox/rt_lights/rt_light.h"


namespace ray_tracing {

class RTSpotLight : public RTLight {
 public:
  // Constructs a sport light defining its position the target it aims, its
  // opening angle. The parameter fuziness (in radians) describes how smooth 
  // the transition is between the lit and the dark area.
  RTSpotLight(Eigen::Vector3d pos,
              Eigen::Vector3d col,
              Eigen::Vector3d target,
              double angle,
              double fuzziness = 0.0)
      : position_(pos),
        color_(col),
        direction_(0.0, 0.0, 0.0),
        angle_(angle),
        fuzziness_(fuzziness) {
    direction_ = target - position_;
    direction_.normalize();
  }

  // Given a point in 3D space (for example the result of the intersection 
  // with a Ray) this function returns the vector from the light to this point
  // (normalized), the light intensity and the light distance.
  void Lighting(Eigen::Vector3d& point,
                Eigen::Vector3d& light_vector,
                Eigen::Vector3d& light_intensity,
                double& light_dist)  {
    light_vector = position_ - point;
    light_dist = light_vector.norm();
    light_vector.normalize();

    double light_vector_angle = acos(-light_vector.dot(direction_));

    if (light_vector_angle <= angle_ - fuzziness_) {
      light_intensity = color_;
    } else if (light_vector_angle >= angle_) {
      light_intensity = Eigen::Vector3d(0.0, 0.0, 0.0);
    } else {
      light_intensity = (angle_ - light_vector_angle) / 
                        std::max(fuzziness_, 0.0001) * color_;
    }

    
  }
   
 private:
  Eigen::Vector3d position_;
  Eigen::Vector3d color_;
  Eigen::Vector3d direction_;
  double angle_;
  double fuzziness_;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_RT_LIGHTS_RT_SPOT_LIGHT_H_