#include <cassert>
#include <iostream>
#include <ray_tracing_sandbox/camera/camera.h>


void ray_tracing::Camera::LookAt(const Eigen::Vector3d camera_position,
                                 const Eigen::Vector3d target_position,
                                 const Eigen::Vector3d up_vector) {
  // ----- Update the camera pose ------------------------------------------ //
  position_ = camera_position;
  
  Eigen::Vector3d z_vector = position_ - target_position; 
  z_vector.normalize();
  Eigen::Vector3d x_vector = up_vector.cross(z_vector); 
  x_vector.normalize();               
  Eigen::Vector3d y_vector = z_vector.cross(x_vector); 
  y_vector.normalize();           
  
  Eigen::Matrix3d transformation_mat;
  transformation_mat.col(0) = x_vector;
  transformation_mat.col(1) = y_vector;
  transformation_mat.col(2) = z_vector;
  
  orientation_ = transformation_mat;  
  
  // ----- Update the pixel positions -------------------------------------- //
  double aspect_ratio = static_cast<double>(width_) /
                        static_cast<double>(height_);
  double pixel_half_size = tan(fovY_/2.0) / 
                           static_cast<double>(height_);
  double pixel_size = 2.0 * pixel_half_size;

  std::vector<double> pixel_vert_coords(height_);
  std::vector<double> pixel_horiz_coords(width_);

  for (int j(0); j<height_; ++j) {
    double tmp_coord = - tan(fovY_/2.0) + pixel_half_size
                       + pixel_size * static_cast<double>(j);
    pixel_vert_coords[j] = tmp_coord;
  }

  for (int i(0); i<width_; ++i) {
    double tmp_coord = - aspect_ratio * tan(fovY_/2.0) + pixel_half_size
                       + pixel_size * static_cast<double>(i);
    pixel_horiz_coords[i] = tmp_coord;
  }

  Eigen::Vector3d pixel_x_size_world = orientation_*
      Eigen::Vector3d(pixel_size, 0.0, 0.0);
  Eigen::Vector3d pixel_y_size_world = orientation_*
      Eigen::Vector3d(0.0, pixel_size, 0.0);

  for (int j(0); j<height_; ++j) {
    for (int i(0); i<width_; ++i) {
      // Cordinates of the pixel in the camera frame
      Eigen::Vector3d pixel_cam_coords(pixel_horiz_coords[i], 
                                       pixel_vert_coords[j], 
                                       -1.0);
      // Cordinates of the pixel in world frame
      Eigen::Vector3d pixel_word_coords = orientation_*pixel_cam_coords 
                                          + position_;
      int pixel_index = j*width_ + i; 
      pixels_[pixel_index].center = pixel_word_coords;
      pixels_[pixel_index].x_size = pixel_x_size_world;
      pixels_[pixel_index].y_size = pixel_y_size_world;
    }
  }                
}

ray_tracing::Pixel ray_tracing::Camera::pixel(int i, int j) const {
  assert((i>=0) && (i<width_));
  assert((j>=0) && (j<height_));

  return pixels_[j*width_ + i];
}