#ifndef RAY_TRACING_SANDBOX_CAMERA_CAMERA_H_
#define RAY_TRACING_SANDBOX_CAMERA_CAMERA_H_

#include <vector>
#include <Eigen/Dense>
#include "ray_tracing_sandbox/base_types/pixel.h"
#include "ray_tracing_sandbox/base_types/ray.h"


namespace ray_tracing {

// Camera class contains the camera pose parameters and 
// pixel positions in space
class Camera {
 public:
  // Constructor
  Camera(int width, int height, double fovY=60.0*M_PI/180.0) 
      : width_(width),
        height_(height),
        fovY_(fovY) {
    position_ = Eigen::Vector3d(0.0, 0.0, 0.0);
    orientation_.Identity(); 
    pixels_.resize(width_*height_);
  }
  
  // CLassic OpenGL LookAt function (see OpenGL documentation for more details)
  void LookAt(const Eigen::Vector3d camera_position,
              const Eigen::Vector3d target_position,
              const Eigen::Vector3d up_vector);
  // Return the pixel (i,j) position in space (with the convention of the image
  // plane at 1m of the camera)
  Pixel pixel(int i, int j) const;
  // Accessor to camera position
  Eigen::Vector3d position() const {return position_;}
  // Accessor to width
  int width() const {return width_;}
  // Accessor to height
  int height() const {return height_;}

 private:
  Eigen::Vector3d position_;
  Eigen::Quaterniond orientation_;
  int width_;
  int height_;
  double fovY_;
  std::vector<Pixel> pixels_;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_CAMERA_CAMERA_H_