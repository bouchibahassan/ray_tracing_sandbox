#ifndef RAY_TRACING_SANDBOX_BASE_TYPES_IMAGE_H_
#define RAY_TRACING_SANDBOX_BASE_TYPES_IMAGE_H_

#include <vector>
#include <string>
#include <Eigen/Dense>


namespace ray_tracing {

enum class PPMFormat : char {
  kASCII,
  kBinary
};


// Container class for images (array of pixels).
// This class provides also export functionalities.
class Image {
 public:
  Image(int width, int height) 
      : width_(width),
        height_(height),
        pixels_(width*height, Eigen::Vector3d(0.0, 0.0, 0.0)) {}

  // Export image to PPM format
  bool ExportPPM(std::string path_to_file,
                 PPMFormat format=PPMFormat::kASCII);

  // Accessor to the pixel (i,j)
  Eigen::Vector3d& pixel(int i, int j) {return pixels_[j*width_ + i];}

 private:
  int width_;
  int height_;
  std::vector<Eigen::Vector3d> pixels_;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_BASE_TYPES_IMAGE_H_