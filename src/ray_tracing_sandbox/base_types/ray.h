#ifndef RAY_TRACING_SANDBOX_BASE_TYPES_RAY_H_
#define RAY_TRACING_SANDBOX_BASE_TYPES_RAY_H_

#include <Eigen/Dense>


namespace ray_tracing {

// A ray is made of a point (from where it have been emited) and 
// a direction (should be normalized)
struct Ray {
  Eigen::Vector3d origin;
  Eigen::Vector3d direction;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_BASE_TYPES_RAY_H_