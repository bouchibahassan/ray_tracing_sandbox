#ifndef RAY_TRACING_SANDBOX_BASE_TYPES_PIXEL_H_
#define RAY_TRACING_SANDBOX_BASE_TYPES_PIXEL_H_

#include <Eigen/Dense>


namespace ray_tracing {

// A pixel contains its center position and its size in each direction of image
// plane (x_size and y_size)
struct Pixel {
  Eigen::Vector3d center;
  Eigen::Vector3d x_size;
  Eigen::Vector3d y_size;
};

}  // namespace ray_tracing 

#endif  // RAY_TRACING_SANDBOX_BASE_TYPES_PIXEL_H_