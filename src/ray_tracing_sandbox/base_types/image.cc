#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <ray_tracing_sandbox/base_types/image.h>


bool ray_tracing::Image::ExportPPM(std::string path_to_file, 
                                   PPMFormat format) {
  std::ofstream file_out(path_to_file, std::ios::binary);

  if (!file_out.good()) {
    std::cout << "Error: can't open PPM file " << path_to_file << std::endl;
    return false;
  }

  switch (format) {
    case PPMFormat::kASCII: {
      // Write header
      file_out << "P3" << std::endl;
      file_out << width_ << " " << height_ << std::endl;
      file_out << 255 << std::endl;

      // Write per pixel information
      for (int j(height_-1); j>=0; --j) {
        for (int i(0); i<width_; ++i) {
          for (int k(0); k<3; ++k) {
            pixel(i,j)[k] = std::min(pixel(i,j)[k], 1.0);
            pixel(i,j)[k] = std::max(pixel(i,j)[k], 0.0);
            file_out << static_cast<int>(255.0 * pixel(i,j)[k]) << " ";
          }
        }
        file_out << std::endl;
      }  
      break;
    }
    case PPMFormat::kBinary: {
      // Write header
      file_out << "P6" << std::endl;
      file_out << width_ << " " << height_ << std::endl;
      file_out << 255 << std::endl;

      // Write per pixel information
      size_t size_in_bytes = 3*width_*height_;
      std::vector<uint8_t> binary_image_data;
      binary_image_data.reserve(size_in_bytes);

      for (int j(height_-1); j>=0; --j) {
        for (int i(0); i<width_; ++i) {
          for (int k(0); k<3; ++k) {
            pixel(i,j)[k] = std::min(pixel(i,j)[k], 1.0);
            pixel(i,j)[k] = std::max(pixel(i,j)[k], 0.0);
            binary_image_data.push_back(
              static_cast<uint8_t>(255.0 * pixel(i,j)[k]));
          }
        }
      } 
      
      file_out.write(reinterpret_cast<char*>(binary_image_data.data()),
                     size_in_bytes);
      break;
    }
  }
  return false;  
}
